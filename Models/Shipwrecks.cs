using MongoDB.Bson;

namespace basicmongodbinaspcorenet.Models
{
    public class Shipwrecks
    {
        public ObjectId _id { get; set; }
        public string recrd { get; set; }
        public string vesslterms { get; set; }
        public string feature_type { get; set; }
        public string chart { get; set; }
        public double latdec { get; set; }
        public double londec { get; set; }
        public string gp_quality { get; set; }
        public string depth { get; set; }
        public string sounding_type { get; set; }
        public string history { get; set; }
        public string quasou { get; set; }
        public string watlev { get; set; }
        public double[] coordinates { get; set; }
    }
}