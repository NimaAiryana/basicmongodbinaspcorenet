using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;

namespace basicmongodbinaspcorenet.Controllers
{
    public class HomeController : Controller
    {
        private IMongoCollection<Models.Shipwrecks> _collection { get; set; }
        public HomeController(IMongoClient client)
        {
            var database = client.GetDatabase("sample_geospatial");

            _collection = database.GetCollection<Models.Shipwrecks>("shipwrecks");
        }
        
        public IActionResult Index()
        {
            var list = _collection.Find(find => find.watlev.Contains("dry")).ToList();

            return Content(System.Text.Json.JsonSerializer.Serialize(list));
        }
    }
}